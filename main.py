import pandas as pd
import random
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon
import subprocess
import os

def getList():
	path = "data/" + ex.cbchoices.currentText() + ".csv"
	df = pd.read_csv(path, engine="python", sep=";", quoting=3, skiprows=[1])
	slr = df['Nom'].tolist()
	sc = []
	for i in slr:
		if i:
			nn = i[:-1][1:]
			sc.append(nn)
	return sc

class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'pickStudent'
        self.initUI()
    
    def initUI(self):
        self.setWindowTitle(self.title)

        listeClass = []
        for file in os.listdir("data/"):
        	listeClass.append(file[:-4])
        self.cbchoices = QComboBox(self)
        self.cbchoices.addItems(listeClass)

        self.cg = QPushButton('Create groups', self)
        self.cg.clicked.connect(self.makeList)

        self.pr = QPushButton('Pick random', self)
        self.pr.clicked.connect(self.pickRandom)

        nb = '2 3 4 5 6 7 8 9 10'.split()
        self.cb = QComboBox()
        self.cb.addItems(nb)


        self.lay = QGridLayout()
        self.lay.addWidget(self.cbchoices, 0, 0)
        self.lay.addWidget(self.cb, 0, 1)
        self.lay.addWidget(self.cg, 1, 0)
        self.lay.addWidget(self.pr, 1, 1)
        self.setLayout(self.lay)

        self.show()
        
    def makeList(self):
        liste = getList()
        random.shuffle(liste)
        gn = int(self.cb.currentText())
        rest = len(liste)%gn
        fl = []

        if rest == 0:
            fl = [liste[x:x+gn] for x in range(0, len(liste), gn)]
        else:
            self.errorDivide(rest)
            fl = [liste[x:x+gn] for x in range(0, len(liste), gn)]

        self.printHTML(fl)

    def pickRandom(self):
        liste = getList()
        random.shuffle(liste)
        self.soloprintHTML(liste)

    def errorDivide(self, leftover):

        #Create message box

        self.msgbox = QMessageBox(self)
        self.msgbox.setWindowTitle('Groupes imparfaits')
        self.msgbox.setText("Il va rester %s élève(s) sans groupe" % leftover)
        self.msgbox.exec()

    def printHTML(self, liste):

        soup1="""
        <!DOCTYPE html>
            <html>
                <head>
                    <link href='https://fonts.googleapis.com/css?family=DM Sans' rel='stylesheet'>
                    <style type="text/css">
                        .questions {
                            width: 48.8%;
                            float: left;
                            border: 2px solid powderblue;
                            padding: 5px;
                            font-family: "DM Sans";
                            line-height: 2;
                            margin: 0;
                            display: block;
                            page-break-inside: avoid;
                        }

                        .questions:nth-child(even) {
                            float: left;
                        }

                        .questions:nth-child(odd) {
                            float: right;
                        }
                    </style>
                </head>
                <body>
        
        """
        soup2="""</body></html>"""

        f = open('outputs/HTML_Page.html', 'w+')
        f.write(soup1)

        for i in range(len(liste)):

            f.write('<p class="questions">Groupe {} :<br>\r'.format(i))
            gr = liste[i]

            for p in gr:
                f.write('{}<br>\r'.format(p))

        f.write('<br>')
        f.write(soup2)
        f.close()
        
        url = os.path.abspath('outputs/HTML_Page.html')

        try:
            os.startfile(url)
        except AttributeError:
            try:
                subprocess.call(['open', url])
            except:
                print('Could not open URL')

    def soloprintHTML(self, liste):

        soup1="""
        <!DOCTYPE html>
            <html>
                <head>
                    <link href='https://fonts.googleapis.com/css?family=DM Sans' rel='stylesheet'>
                    <style type="text/css">
                        .questions {
                            width: 48.8%;
                            float: left;
                            padding: 5px;
                            font-family: "DM Sans";
                            line-height: 2;
                            margin: 0;
                            display: block;
                            page-break-inside: avoid;
                        }
                    </style>
                </head>
                <body>
        
        """
        soup2="""</body></html>"""

        f = open('outputs/HTML_Page_Solo.html', 'w+')
        f.write(soup1)
        f.write('<p class="questions"> La personne selectionnée est :<br> {}<br>\r'.format(liste[0]))
        f.write('<br>')
        f.write(soup2)
        f.close()
        
        url = os.path.abspath('outputs/HTML_Page_Solo.html')
        
        try:
            os.startfile(url)
        except AttributeError:
            try:
                subprocess.call(['open', url])
            except:
                print('Could not open URL')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
